'use strict'

// setting all the routes of the Single page application client using AngularJS ui-router
angular.module('InternshipProcessClient.routes',['ui.router'])
    .config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');
        $stateProvider

            .state('home', {
                url: '/home',
                templateUrl: 'app/views/pages/home.html',
                authenticated: false
            })
            .state('dashboard',{
                url:'/dashboard',
                templateUrl:'app/views/pages/dashboard.html',
                controller:'loginCtrl as user',
                authenticated:true
            })
            .state('dashboard.form1',{
                url:'/form1',
                templateUrl:'app/views/pages/forms/form1.html',
                controller:'form1Ctrl as form1',
                authenticated:true
            })
            .state('dashboard.form3',{
                url:'/form3',
                templateUrl:'app/views/pages/forms/form3.html',
                controller:'form3Ctrl as form3',
                authenticated:true
            })

            .state('login',{
                url:'/login',
                templateUrl:'app/views/pages/user/login.html',
                controller:'loginCtrl as login',
                authenticated:false
            })
            .state('signup',{
                url:'/signup',
                templateUrl:'app/views/pages/user/signup.html',
                controller:'signUpCtrl as signup',
                authenticated:true
            })

    });