angular.module('userController').service('authService',['$window','$http',function ($window,$http) {
    this.login=function (loginData) {
        return $http.post('http://localhost:7070/user/authenticate',loginData);
    };

    this.setUser=function (token) {
        $window.localStorage.setItem('username',token.username);
        $window.localStorage.setItem('designation',token.designation);
        $window.localStorage.setItem('studentId',token.studentId);
    };

    this.getUser=function () {
        var token={
            username : $window.localStorage.getItem('username'),
            designation : $window.localStorage.getItem('designation'),
            studentId : $window.localStorage.getItem('studentId')
        };
        return token;
    };

    this.logout=function () {
        $window.localStorage.removeItem('username');
        $window.localStorage.removeItem('designation');
        $window.localStorage.removeItem('studentId');
    }
}]);