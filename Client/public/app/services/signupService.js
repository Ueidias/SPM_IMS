angular.module('signupService', [])

    .factory('UserSer', ['$http', function ($http) {
        const userFactory = [];

        //signs up the user by posting given credentials
        userFactory.signUp = function (Data) {
            return $http.post('http://localhost:7070/user/addUser', Data)
        }

        return userFactory;
    }])