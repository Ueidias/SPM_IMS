angular.module('form1Factory',[])
    .factory('form1',['$http',function ($http) {
        var form1Factory = [];

        //save formi1 student data
        form1Factory.insertForm1StudentData = function (data) {
            return $http.post('http://localhost:7070/formi1/addFormI1Details',data).then(function (data2) {
                return data2;
            })
        };

        //save formi1 supervisor data
        form1Factory.insertForm1SupervisorData = function (id,data) {
            return $http.put('http://localhost:7070/formi1/updateFormI1Details/'+id,data).then(function (data2) {
                return data2;
            })
        };

        //retrieve form 1 details
        form1Factory.viewFormi1Data = function (id) {
            return $http.get('http://localhost:7070/formi1/getFormI1Details/'+id).then(function (data2) {
                return data2;
            })
        };

        //Send email to notify that student has submitted form i1
        form1Factory.SendForm1ByEmail = function (data) {
            return $http.post('http://localhost:7070/form1email/sendmail/form1',data).then(function (data2) {
                return data2;
            })
        };

        return form1Factory;
    }]);