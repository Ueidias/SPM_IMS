angular.module('form1Controller',[])

    .controller('form1Ctrl',['form1','$scope','$rootScope','authService',function (form1,$scope,$rootScope,authService) {
        const app = this;
        app.retrieveFormi1Data = retrieveFormi1Data();
        app.form1StudentBody = [];
        app.form1SupervisorBody = [];

        app.studentDone = function () {
            app.lockForm1studentButton = true;
            app.form1StudentBody = {
                "studentID":app.studentID,
                "studentName":app.studentName,
                "studentAddress": app.studentAddress,
                "studentHomePhone":app.studentHomePhone,
                "studentMobilePhone":app.studentMobilePhone,
                "studentEmails":app.studentEmails,
                "semester":app.semester,
                "year":app.year,
                "empName":null,
                "empAddress":null,
                "supName":null,
                "supPhone": null,
                "supTitle":null,
                "supEmail":null,
                "intStartDate":null,
                "intEndDate": null,
                "numOfHrs":null,
                "listOfexpectedTasks":null,
                "listWhatStudentLearn":null
            };

            form1.insertForm1StudentData(app.form1StudentBody).then(function (res) {
                console.log(res);
            })
        };

        app.supervisorDone = function () {
            app.lockForm1supervisorButton = true;
            app.form1SupervisorBody = {
                "empName":app.empName,
                "empAddress":app.empAddress,
                "supName":app.supName,
                "supPhone": app.supPhone,
                "supTitle":app.supTitle,
                "supEmail":app.supEmail,
                "intStartDate":app.intStartDate,
                "intEndDate": app.intEndDate,
                "numOfHrs": app.numOfHrs,
                "listOfexpectedTasks":app.listOfexpectedTasks,
                "listWhatStudentLearn":app.listWhatStudentLearn
            };

            confirmForm1ByEmail();
            form1.insertForm1SupervisorData(app.studentID,app.form1SupervisorBody).then(function (res) {
                console.log(res);
            })
        };

        function confirmForm1ByEmail() {
            app.form1EmailBody = {
                "studentID":app.studentID,
                "studentName":app.studentName,
                "studentAddress": app.studentAddress,
                "studentHomePhone":app.studentHomePhone,
                "studentMobilePhone":app.studentMobilePhone,
                "studentEmails":app.studentEmails,
                "semester":app.semester,
                "year":app.year,
                "empName":app.empName,
                "empAddress":app.empAddress,
                "supName":app.supName,
                "supPhone": app.supPhone,
                "supTitle":app.supTitle,
                "supEmail":app.supEmail
            };

            form1.SendForm1ByEmail(app.form1EmailBody).then(function (res) {
                console.log(res);
            })
        }

        function retrieveFormi1Data() {
            let studentID = authService.getUser().studentId;

            form1.viewFormi1Data(studentID).then(function (res) {
                var retrievedData=res.data.data[0];

                if ((retrievedData.userID == "")||(retrievedData.userID == null)){
                        app.lockForm1studentButton = false;
                    }else{
                        app.lockForm1studentButton = true;
                    }
                if ((retrievedData.employername == "")||(retrievedData.employername == null)){
                    app.lockForm1supervisorButton = false;
                }else{
                    app.lockForm1supervisorButton = true;
                }

                    app.studentID = retrievedData.userID,
                    app.studentName = retrievedData.studentName,
                    app.studentAddress = retrievedData.studentAddress,
                    app.studentHomePhone = retrievedData.studentHomePhone,
                    app.studentMobilePhone = retrievedData.studentMobilePhone,
                    app.studentEmails = retrievedData.studentEmails,
                    app.semester = retrievedData.semester,
                    app.year = retrievedData.year,
                    app.empName = retrievedData.employername,
                    app.empAddress = retrievedData.employeraddress,
                    app.supName = retrievedData.supervisorName,
                    app.supPhone = retrievedData.supervisorPhone,
                    app.supTitle = retrievedData.supervisorTitle,
                    app.supEmail = retrievedData.supervisorEmail,
                    app.intStartDate = retrievedData.internstartdate,
                    app.intEndDate = retrievedData.internenddate,
                    app.numOfHrs = retrievedData.noofhours,
                    app.listOfexpectedTasks = retrievedData.tasktocomplete,
                    app.listWhatStudentLearn = retrievedData.whatwilllern
            })
        };
    }]);