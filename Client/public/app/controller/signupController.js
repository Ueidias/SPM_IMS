angular.module('signupController', [])

    .controller('signUpCtrl', ['UserSer', '$timeout', '$location', '$window', function (UserSer, $timeout, $location, $window) {
        const app = this;

        //signs up a new user by using the user service
        app.signupUser = function (signupdata) {
            signupdata.permission = "user";

            UserSer.signUp(signupdata).then(function (data) {
                if (data.data) {
                    app.errorMessage = null;
                    app.success = true;
                    app.successMessage = data.data.message + " Redirecting to login...";
                    $timeout(function () {
                        $location.path('/home');
                    }, 2000);
                } else {
                    app.errorMessage = data.data.message;
                }
            }).catch(function (err) {
                app.success = false;
                app.errorMessage = "Username already exists!";
            })
        }

    }])