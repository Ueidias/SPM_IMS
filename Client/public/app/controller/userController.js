angular.module('userController',[])
    .controller('loginCtrl', ['$location','authService', function($location,authService){
        const app = this;

        //validate and authenticate login
        app.validateLogin = function(loginData){
            console.log('validateLogin function triggered');
            authService.login(loginData).then(function (response) {

                if(response.data.authenticated==true){

                    $location.path('/dashboard');
                    authService.setUser(response.data);

                    console.log('User authenticated successfully');
                }else {
                    app.success=false;
                    app.loginMessage = 'Invalid Credentials! Please try again';

                    console.log('User authentication failed');
                }
            });
        };

        //logout user from the client
        app.logout = function () {
            authService.logout();
            $location.path('/home');

            console.log('User logged out successfully')
        };

        app.currentUser=authService.getUser();
    }]);



