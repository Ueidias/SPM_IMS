'use strict'

// Main application of the client
// this has all the dependencies of other modules
angular.module('InternshipProcessClient',[
	'InternshipProcessClient.routes',
	'form1Controller',
	'form1Factory',
    'signupService',
    'signupController',
    'form3Controller',
    'form3Factory',
	'userController'


])
//TODO:ADD AUTHENTICATIONS HERE
