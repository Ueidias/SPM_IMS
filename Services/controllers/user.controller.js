'use strict';

const express=require('express');
const router=express.Router();
const userModel = require('../models/user.model');

router.post('/authenticate',function (req,res) {
    userModel.authenticateUser(req.body).then(result=>{
        res.status(200).send(result);
        console.log('User validate function executed successfully');
    }).catch(err=>{
        res.status(500).send(err);
        console.error(err);
    });
});

router.post('/addUser',function (req,res) {
    userModel.addUser(req.body).then(result=>{
        res.status(201).send(result);
        console.log('User created successfully')
    }).catch(err=>{
        res.status(500).send(err);
        console.error(err);
    })
});

module.exports=router;