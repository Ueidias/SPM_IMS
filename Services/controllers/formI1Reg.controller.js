'use strict';

const express=require('express');
const router=express.Router();
const formI1Model = require('../models/formI1Reg.model');

router.post('/addFormI1Details',function (req,res) {
    formI1Model.addFormI1(req.body).then(result=>{
        console.log(result);
        res.send(result);
    }).catch(err=>{
        res.send(err);
    })
});

router.put('/updateFormI1Details/:id',function (req,res) {
    formI1Model.updateFormI1(req.params.id, req.body).then(result=>{
        console.log(result);
        res.send(result);
    }).catch(err=>{
        res.send(err);
    })
});

router.get('/getFormI1Details/:studId',function (req,res) {
    var studentid = req.params.studId;
    formI1Model.getFormIlDetails(studentid).then(result=>{
        console.log(result);
        res.send(result);
    }).catch(err=>{
        res.send(err);
    })
});

module.exports=router;