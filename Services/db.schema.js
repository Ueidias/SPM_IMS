'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

mongoose.connect('mongodb://localhost:27017/sliit_ims').then(function () {
    console.log('Connected to MongoDB');
}).catch(err=>{
    console.log(err);
});

const registerSchema= new Schema({
    studentId: {
        type: String
    },
    designation: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    fname: {
        type: String
    },
    lname: {
        type: String
    },
    password: {
        type: String,
        required: true
    },
    permission: {
        type: String
    },
    username: {
        type: String,
        required: true,
        unique:true
    },
});

const form1Schema=new Schema({
    userID:{type:String},
    studentName:{type:String},
    studentAddress:{type:String},
    studentHomePhone:{type:String},
    studentMobilePhone:{type:String},
    studentEmails:{type:String},
    semester:{type:String},
    year:{type:String},
    employername:{type:String},
    employeraddress:{type:String},
    supervisorName:{type:String},
    supervisorPhone:{type:String},
    supervisorTitle:{type:String},
    supervisorEmail:{type:String},
    internstartdate:{type:String},
    internenddate:{type:String},
    noofhours:{type:String},
    tasktocomplete:{type:String},
    whatwilllern:{type:String}
});

const form3Schema=new Schema({

    InternshipTitle:{type:String},
    Specialisation:{type:String},
    OverallInternshipPeriodFrom:{type:String},
    PeriodTo:{type:String},
    TrainingParty:{type:String},
    TrainingDescription:{type:String},
    PeriodFrom:{type:String},
    Periodto:{type:String},
    ExternalSupervisorName:{type:String},
    Date:{type:String},
    ExternalSupervisorName1:{type:String},
    Date2:{type:String}

});

const summerySchema=new Schema({

    summary1:{type:String},
    summary2:{type:String},
    summary3:{type:String},
    summary4:{type:String},
    summary5:{type:String},
    summary6:{type:String}
});

const detailsSchema=new Schema({

    DetailsOfWork1:{type:String},
    DetailsOfWork2:{type:String},
    DetailsOfWork3:{type:String},
    DetailsOfWork4:{type:String},
    DetailsOfWork5:{type:String},
    DetailsOfWork6:{type:String}
});

const remarkSchema=new Schema({

    Remarks1:{type:String},
    Remarks2:{type:String},
    Remarks3:{type:String},
    Remarks4:{type:String},
    Remarks5:{type:String},
    Remarks6:{type:String}
});

mongoose.model('registrations',registerSchema);
mongoose.model('form_I1_Details',form1Schema);
mongoose.model('form_I3_Details',form3Schema);
mongoose.model('form_I3_Details',summerySchema);
mongoose.model('form_I3_Details',detailsSchema);
mongoose.model('form_I3_Details',remarkSchema);

