const express=require('express');
const routes=express.Router();
const formI1route = require('./controllers/formI1Reg.controller');


routes.use('/formI1',formI1route);
routes.use('/user',require('./controllers/user.controller'));
routes.use('/form1email',require('./controllers/form1.email.controller'));

module.exports=routes;