'use strict';
const mongoose = require('mongoose');
require('../db.schema');

exports.authenticateUser=function (body) {
    const registrationModel = mongoose.model('registrations');
    return new Promise(function (res,rej) {
        registrationModel.find({username:body.username,password:body.password}).exec().then(data=>{
            if(data.length==0){
                res({
                    authenticated : false
                });
            }else{
                res({
                    studentId : data[0].studentId,
                    username : data[0].username,
                    designation : data[0].designation,
                    authenticated : true
                });
            }
        }).catch(err=>{
            rej(err);
        });
    })
};

exports.addUser=function (body) {
    return new Promise(function (resolve, reject) {
        const registrationModel = mongoose.model('registrations');
        let newUser = new registrationModel(body);
        newUser.save().then(data=>{
            resolve(data);
        }).catch(err=>{
            reject(err);
        });
    });
};