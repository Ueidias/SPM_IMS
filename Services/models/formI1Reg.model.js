'use strict';
const mongoose = require('mongoose');
require('../db.schema');


exports.addFormI1=function (stdInstance) {
    return new Promise(function (resolve, reject) {
        const formI1 = mongoose.model('form_I1_Details');
        let newForm = new formI1({
            userID : stdInstance.studentID,
            studentName : stdInstance.studentName,
            studentAddress : stdInstance.studentAddress,
            studentHomePhone : stdInstance.studentHomePhone,
            studentMobilePhone : stdInstance.studentMobilePhone,
            studentEmails : stdInstance.studentEmails,
            semester : stdInstance.semester,
            year : stdInstance.year,
            employername : stdInstance.employername,
            employeraddress : stdInstance.employeraddress,
            supervisorName : stdInstance.supervisorName,
            supervisorPhone : stdInstance.supervisorPhone,
            supervisorTitle : stdInstance.supervisorTitle,
            supervisorEmail : stdInstance.supervisorEmail,
            internstartdate : stdInstance.internstartdate,
            internenddate : stdInstance.internenddate,
            noofhours : stdInstance.noofhours,
            tasktocomplete : stdInstance.tasktocomplete,
            whatwilllern : stdInstance.whatwilllern
        }
        );
        newForm.save().then(function () {
            resolve({status:200,message:'Student details added successfully'});

        }).catch(function (reason) {
            reject({status:500,message:"Error" + reason});
        });
    });
};

exports.updateFormI1= function(id,data){
    return new Promise(function(Resolve,Reject){
        const formI1 = mongoose.model('form_I1_Details');

        let newForm ={
            employername : data.empName,
            employeraddress : data.empAddress,
            supervisorName : data.supName,
            supervisorPhone : data.supPhone,
            supervisorTitle : data.supTitle,
            supervisorEmail : data.supEmail,
            internstartdate : data.intStartDate,
            internenddate : data.intEndDate,
            noofhours : data.numOfHrs,
            tasktocomplete : data.listOfexpectedTasks,
            whatwilllern : data.listWhatStudentLearn
        };

        formI1.updateMany({userID : id}, newForm).exec().then(function (value) {
            Resolve({status:200,message : "FormI1 Updated Successfully"});
        }).catch(function(err){
            Reject({status:500, message : "FormI1 cannot be updated, Error : " + err});
        })
    });
};

exports.getFormIlDetails = function(studentid){
    return new Promise(function (resolve,reject) {
        const formI1 = mongoose.model('form_I1_Details');
        formI1.find({userID:studentid}).exec().then(function (value) {
            resolve({status:200,"data":value});
        }).catch(function (reason) {
            reject({status:500, message: "Cannot retrieve any Details, Error : " + reason});
        })

    });
};